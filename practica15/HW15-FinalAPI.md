# PRÁCTICA NRO15 ✨

- **DOCENTE:** Ing. David Mamani Figueroa 🎓
- **AUXILIAR:** Univ. Sergio Moises Apaza Caballero 📚
- **Universitario:** Alex Jaime Beltran Tarqui 📝
- **SIGLA:** SIS-313 🖥️
- **GRUPO:** 1️⃣

---

## Mensajes de GET, POST y PUT 🌐

- **GET**: Solicita datos de un recurso específico. Es como pedir información al servidor, como una página web o datos particulares.
- **POST**: Envía datos nuevos al servidor. Se usa para enviar información, por ejemplo, al enviar un formulario en línea.
- **PUT**: Actualiza datos existentes en el servidor. Se utiliza para modificar o actualizar información que ya está almacenada.

---

## Propósito de los códigos de respuesta HTTP 📡

Los códigos de respuesta HTTP indican el resultado de una solicitud al servidor:

- **(200-299)**: Indican que la solicitud fue exitosa.
- **(300-399)**: Indican redirecciones.
- **(400-499)**: Indican errores causados por el cliente.
- **(500-599)**: Indican errores internos del servidor.

---

## Clases y rangos de códigos de respuesta HTTP 🚦

- **(100-199)**: Respuestas informativas.
- **(200-299)**: Respuestas exitosas.
- **(300-399)**: Redirecciones.
- **(400-499)**: Errores del cliente.
- **(500-599)**: Errores del servidor.

---

## Explicación de códigos de respuesta HTTP específicos ℹ️

- **200 OK**: Todo salió bien, la solicitud se completó correctamente.
- **400 Bad Request**: Hubo un error en la solicitud que el servidor no entendió.
- **401 Unauthorized**: Necesitas identificarte (como iniciar sesión) para acceder a la solicitud.
- **403 Forbidden**: No tienes permiso para acceder al recurso solicitado.
- **404 Not Found**: El recurso que estás buscando no se encuentra en el servidor.
- **408 Request Timeout**: El servidor esperó demasiado tiempo para recibir la solicitud.
- **500 Internal Server Error**: Algo salió mal en el servidor mientras intentaba completar la solicitud.
- **501 Not Implemented**: El servidor no soporta la funcionalidad necesaria para completar la solicitud.
- **502 Bad Gateway**: El servidor actuando como puerta de enlace o proxy recibió una respuesta no válida.
- **504 Gateway Timeout**: El servidor actuando como puerta de enlace o proxy no pudo obtener una respuesta a tiempo.

---

## Definición de endpoint 🔗


Un endpoint es como una dirección específica en internet que usas para acceder a un servicio en línea. Es la parte final de una URL que te permite interactuar con una API o servicio web, como enviar mensajes, obtener datos o realizar acciones específicas.