export default function FooterLink(props:{title:string, item:string[]}){
    return(
        <div className="footer-link">
        <h3>{props.title}</h3>
        <ul>
        {props.item.map((item:string,index:number)=>{
                    return <li key={index}>
                        <a href="#">{item}</a>
                    </li>
                })}
        </ul>
        </div>
    )
}