import TituloSeccion from "./Discover"
import Button from "./Button"
export default function Header() {
    return (
        <div className="text-container">
            <TituloSeccion text="REAL ESTATE"></TituloSeccion>
            <div className="text-descripcion">
                <h1>Let<sup>,</sup>s hunt for your dream residence</h1>
            <p>Explore our range of beautiful properties with the addition of separate accommodation suitable for you.</p>
            </div>
            <div className="btn1">
                <Button  texto="Bun"></Button>
                <Button  texto="Rent"></Button>
            </div>
        </div>
            )
            
}