import Button from "./Button";
import LogoHeader from "./LogoHeader";
const Navbar = () => {
    return (
        <header className="Navbar">
            <LogoHeader imgUrl="/img/logoHeader.svg" titulo="Mulih"></LogoHeader>
            <nav>
                <ul>
                    <li><a href="#home">Home</a></li>
                    <li><a href="#property">Property</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#service">Service</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </nav>
            <div className="btn1">
                <Button texto="Sign in"></Button>
                <Button texto="Login"></Button>
            </div>
        </header>
    );
}
export default Navbar;