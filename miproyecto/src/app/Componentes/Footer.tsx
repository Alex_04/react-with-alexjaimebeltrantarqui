import LogoHeader from "./LogoHeader"
import Image from "next/image";
import Button from "./Button"
import FooterLink from "./FooterLink";
const Footer = () => {
    return(
        <footer className="footer">
            <div className="footer-content">
                <div className="footer-about">
                <LogoHeader imgUrl="/img/logoHeader.svg" titulo="Mulih"></LogoHeader>
                <p>We have built our reputation as true local area experts.</p>
                <div className="newsletter">
                    <h3>Newsletter</h3>
                    <input type="email" placeholder="Input your email " />
                    <Button texto="Send"></Button>
                </div>  
                </div>
                <div className="footer-links">
           <FooterLink title="Service" item={["About us", "Contact", "Terms & Conditions", "Privacy & Policy"]}></FooterLink>
           <FooterLink title="Check This" item={["Find agents", "Lifestyle"]}></FooterLink>
           <div className="footer-icon">
            <h3>Follow us <br /> on</h3>
            <div>
                 <Image src="/img/icon1.svg" alt="" width={26} height={26} />
                 <Image src="/img/icon2.svg" alt="" width={26} height={26}/>
                 <Image src="/img/icon3.svg" alt="" width={26} height={26}/>
            </div>
           </div>
            </div>
            </div> 
        </footer>
    );
};
export default Footer;