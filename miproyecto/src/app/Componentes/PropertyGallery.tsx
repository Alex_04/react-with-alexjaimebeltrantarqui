import Image from "next/image"
import Button from "./Button"
export default function PropertyGallery(props: { imgUrl: string, titulo: string, lugar: string, precio: string }) {
    return (
        <div className="card">
            <Image className="card-img" src={props.imgUrl} alt="" width={100} height={100} />
            <div className="card-content">
                <h3>{props.titulo}</h3>
                <p>{props.lugar}</p>
                <div className="precio-content">
                    <p id="precio">{props.precio}</p>
                    <p className="area">360m<sup>2</sup>  Living Area</p>
                </div>
                <div className="btn3">
                    <Button texto="Send Inquiry"></Button>
                </div>
            </div>
        </div>
        
    )
}