import Image from "next/image";

export default function LogoHeader(props:{imgUrl: string, titulo: string}) {
    return (
        <div className="logo-principal">
            <Image src={props.imgUrl}  alt="" width={35} height={35}/>
            <span>{ props.titulo}</span>
        </div>
    )
}
