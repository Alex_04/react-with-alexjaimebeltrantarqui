import Image from "next/image"
export default function CardAdvantage(props: { imgUrl: string, nombre: string, descripcion: string }) {
    return (
        <div className="card-advantage">
            <Image src={props.imgUrl} alt="" width={50} height={50} />
            <h1>{props.nombre}</h1>
            <p>{ props.descripcion}</p>
        </div>
        
    )
}