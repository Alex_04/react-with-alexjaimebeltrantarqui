import Button from "./Button"
export default function TituloSeccion(props: { text: string, titulo?: string, descripcion?: string }) {
    return (
        <div className="titulo">
            <div className="text-titulo">
                <p id="color-titulo">{props.text}</p>
            </div>
            <h1>{props.titulo}</h1>
            <p>{props.descripcion}</p>
        </div>
        
        
        
       
    )
}
