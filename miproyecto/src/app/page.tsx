import Image from "next/image";
import styles from "./page.module.css";
import Navbar from "./Componentes/Navbar";
import Header from "./Componentes/Header";
import TituloSeccion from "./Componentes/Discover";
import PropertyGallery from "./Componentes/PropertyGallery";
import Button from "./Componentes/Button";
import CardAdvantage from "./Componentes/CardAdvantage";
import Footer from "./Componentes/Footer";


export default function Home() {
  return (
    <main className={styles.main}>
       <div className="img-container">
          <Image src="/img/img1.svg" alt="" width={806} height={760}/>
        </div>
      <Navbar></Navbar>
      <div className="seccion1">
        <Header></Header>
        </div>
      <div className="seccion2">
        <div className="primero">
          <TituloSeccion text="DISCOVER" titulo="Best recomendation" descripcion="Discover our exclusive selection of the finest one-of-a-kind luxury properties architectural masterpieces."></TituloSeccion>
          <div className="btn2">
            <Button texto="Learn More"></Button>
            <Image className="iconly" src="/img/iconly.svg" alt="" width={28} height={28}/>
          </div>
        </div>
        <div className="seccion-card">
          <PropertyGallery imgUrl="/img/card1.svg" titulo="Perum griya asri" lugar="Bogor, Jawa Barat" precio="$25,000"></PropertyGallery>
          <PropertyGallery imgUrl="/img/card2.svg" titulo="Perum kencana asri" lugar="Bogor, Jawa Barat" precio="$25,000"></PropertyGallery>
          <PropertyGallery imgUrl="/img/card3.svg" titulo="Perum rakjel elit" lugar="Bogor, Jawa Barat" precio="$25,000"></PropertyGallery>
        </div>
        </div>
      <div className="advantages-section">
        <TituloSeccion text="OUR ADVANTE" titulo="Giving you peace of mind" ></TituloSeccion>
        <div className="advantage-container">
          <CardAdvantage imgUrl="/img/advantage1.svg" nombre="Comfortable" descripcion="Enjoy lifestyle amenities designed to provide every homeowners modern comfort, a stone's throw away from schools, churches, and hospitals."></CardAdvantage>
          <CardAdvantage imgUrl="/img/advantage2.svg" nombre="Extra security" descripcion="You can connect with potential tenants without having to share your phone number. We also require all users to register to validate their legitimacy."></CardAdvantage>
          <CardAdvantage imgUrl="/img/advantage3.svg" nombre="Luxury" descripcion="Find out how we provide the highest standard of professional property management to give you all the benefits of property."></CardAdvantage>
          <CardAdvantage imgUrl="/img/advantage4.svg" nombre="Best price" descripcion="Not sure what you should be charging for your property? Let us do the numbers for you—contact us today for a free rental appraisal on your home."></CardAdvantage>
          <CardAdvantage imgUrl="/img/advantage5.svg" nombre="Strategic location" descripcion="located in the city center close to the shopping center. Very good for areas surrounded by international education centers, start-up office centers."></CardAdvantage>
          <CardAdvantage imgUrl="/img/advantage6.svg" nombre="Efficient" descripcion="Schedule visits to multiple properties at once in one day without having to call them one by one. Check everything and find the best properties for rent."></CardAdvantage>
        </div>
        </div>
      <Footer></Footer>
    </main>
  );
}
