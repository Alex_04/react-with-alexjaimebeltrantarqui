import Image from "next/image";
import styles from "./page.module.css";
import Login from "./Component/Login"; 
import Signup from "./Component/SignUp";


export default function Home() {
  return (
    <main className={styles.main}>
      <Login></Login>
      <Signup></Signup>
     
    </main>
  );
}
