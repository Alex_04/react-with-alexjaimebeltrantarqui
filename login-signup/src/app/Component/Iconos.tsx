export default function Iconos(props:{imgUrl: string}) {
    return (
        
        <div className="icon">
            <img  width={28} height={28} src={props.imgUrl} alt="" />
            </div>
           
            
    )
}