<<<<<<< HEAD
<<<<<<< HEAD
import Link from 'next/link';
=======
>>>>>>> d777d429649feb5621a092bf2c405c700dd76127
=======
>>>>>>> d777d429649feb5621a092bf2c405c700dd76127
import React from 'react';
import Button from './Button';
import Iconos from './Iconos';
const Login = () => {
    return (
        <div className="login-contenedor">
            <div className="login-form">
                <form action="">
                    <h1>Login</h1>
                    <p>Login to access your travelwise  account</p>
                    <div className="input-box">
                        <label>Email:</label>
                        <input type="text" name="email" id="email" required />
                    </div>
                    <div className="input-box">
                        <label>Password:</label>
                        <input type="password" name="password" id="password" required />
                    </div>
                    <div className='checkbox'>
                        <input type="checkbox" name="remember-me" id="remember-me" />
                        <label>remember-me</label>
                        <a href="">Forgot Password</a>
                    </div>
                    <div>
                        <Button nombre='Login'texto='Don’t have an account?' direccion='Sing up'></Button>
                    </div>
                </form>
                <div className='contenedor'>
                    <div className='linea'></div>
                    <span>Or login with</span>
                </div>
                <div className='icon-contenedor'>
                    <Iconos imgUrl='/img/icon1.svg'></Iconos>
                    <Iconos imgUrl='/img/icon2.svg'></Iconos>
                    <Iconos imgUrl='/img/icon3.svg'></Iconos>
                </div>
            </div>
            <img width={400} height={500} src="/img/img-login.svg" alt="" />
        </div>
     
    );
}

export default Login;