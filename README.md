# 🏡 PROYECTO DE REACT AUX-SIS313

**Creado por:** Univ. Beltran Tarqui Alex Jaime

## 🔗 LINK DEL TEMPLATE EN FIGMA

- [🎨 Template en Figma](https://www.figma.com/design/QIxFzh0DdzSGX4LKiNtxPy/Real-Estate---Landing-Page-(Freebies)-(Community)?node-id=2-22&t=NidHeViEYIZpe1P6-0)

## 🔗 LINK DEL REPOSITORIO PRINCIPAL

- [📂 Repositorio Principal](https://gitlab.com/Alex_04/react-with-alexjaimebeltrantarqui)

## 🔗 LINK DE NETLIFY

- [🌐 Link de Netlify](https://main--teal-panda-fecd3b.netlify.app/) 

## 🛠️ Avances del Template

- [x] **Navbar**
- [x] **Main Header**
- [x] **Recommendations Section**
- [x] **Advantages Section**
- [ ] **Customer Testimonials Section**
- [ ] **Rating and Stats Section**
- [ ] **Company Info Section**
- [x] **Footer**

***

**¡HASTA PRONTO!** 😊
