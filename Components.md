## COMPONENTES

1. **Navbar** 🖥️  
2. **Main Header**🏠 
3. **Recommendations Section** 📋
4. **Advantages Section** 💼
5. **Customer Testimonials Section** 🗣️
6. **Rating and Stats Section** ⭐
7. **Company Info Section** ℹ️
8. **Footer** 📌
***
**¡HASTA PRONTO!** 😊






